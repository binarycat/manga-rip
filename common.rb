require 'nokogiri'
require 'net/http'
require 'json'
require './chapterindex.rb'

def get_doc(url_str)
  u = URI(url_str)
  doc = Nokogiri::HTML.parse(Net::HTTP.get u)
  return doc
end

def spoof_referer(url, referer)
  req = Net::HTTP::Get.new url
  req['Referer'] = referer
  res = Net::HTTP.start(url.hostname, url.port, use_ssl: url.scheme == 'https') { |http|
    http.request(req)
  }
  # TODO: thow error if we don't get a 200 OK response
  return res.body
end

# common code for scraping a chapter list
def rip_chapters(url_str, xp, fn)
  doc = get_doc(url_str)
  ChapterIndex.new(doc.xpath(xp)) { |elem|
    method(fn).call(URI.join(url_str, elem.value).to_s)
  }.reverse
end

# yes i'm monkeypatching, sue me
class Nokogiri::XML::NodeSet
  def values = map{|x|x.value}
end
