require 'net/http'
require './common.rb'



class Handler
  @@table = []
  def initialize(pre)
    @prefix = pre
    @@table << self
  end
  def match?(url_str)
    url_str.start_with? @prefix
  end
  def download_page(url)
    return Net::HTTP.get url
  end
  # for diagnostic purpouses
  def site = @prefix
  def Handler.table
    return @@table
  end
end

# handler that spoofs the Referer header
class SpoofHandler < Handler
  # this can be overridden by an instance method if
  # the required referer value is different from the url prefix
  def referer = @prefix
  def download_page(url)
    return spoof_referer(url, referer)
  end
end

require './site/dynasty-scans.rb'
require './site/manga4life.rb'
require './site/webtoons.rb'
require './site/mangapill.rb'
require './site/mangabuddy.rb'
require './site/manganato.rb'
require './site/mangakakalot.rb'

# scrapes the urls of all the images in a manga.
# URL -> [[URL]]
def rip_manga(url_str)
  hdr = dispatch_manga(url_str)
  return hdr.rip url_str
end

def dispatch_manga(url_str)
  Handler.table.each { |hdr|
    if hdr.match?(url_str) then
      return hdr
    end
  }
  return nil
end

# outdir must already exist
# the first chapter is chapter #1
def download_manga(url_str, outdir, chapters="all", progress=false)
  # TODO: better error handling.  output a nice "no handler found" error.
  hdr = dispatch_manga(url_str)
  if progress then print "indexing chapters...\n" end
  ch_list = hdr.rip(url_str)
  if chapters == "all" then
    chapters = 1..ch_list.length
  end
  chapters.each { |i|
    ch_no = i
    ch_dir = File.join(outdir, "ch"+ch_no.to_s.rjust(3, '0'))
    Dir.mkdir ch_dir
    # adjust from 1-indexed chapter numbers to zero-indexed array position
    chapter = ch_list[i-1]
    if progress then print "chapter #{ch_no} has #{chapter.length} pages\n" end
    chapter.each_with_index {|page_url, pg_no|
      pg_no += 1
      if progress then print "downloading ch #{ch_no} pg #{pg_no}\n" end 
      body, type = hdr.download_page URI(page_url)
      # TODO: determine file extension from mime type
      File.open(File.join(ch_dir, pg_no.to_s.rjust(3, '0')), "w") { |file|
        file.write(body)
      }
    }
  }
end

def get_supported_sites()
  Handler.table.map {|hdr|
    hdr.site
  }
end
