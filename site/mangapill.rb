require './common.rb'

def mangapill_rip_chapter(url_str)
  doc = get_doc(url_str)
  return doc.xpath('//chapter-page//img/@data-src').map{|x|x.value}
end

def mangapill_rip_series(url_str) = rip_chapters(
  url_str, '//div[@id="chapters"]/div/a/@href', :mangapill_rip_chapter)

mangapill_h = Handler.new "https://mangapill.com/manga/"

def mangapill_h.rip(url_str) = mangapill_rip_series(url_str)

def mangapill_h.download_page(url)
  return spoof_referer(url, "https://mangapill.com/") 
end
