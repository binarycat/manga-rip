require './common.rb'

def dynasty_scans_rip_chapter(url_str)
  doc = get_doc(url_str)
    # chapter info is stored in a javascript assignment, extract it
  script = doc.xpath("//script[not(@src)]").last.inner_text
  #ch_title = doc.xpath('//h3[@id="chapter-title"]/b').inner_text
  #chapter = Chapter.new url_str, ch_title
  JSON.parse(script[31..-11]).map { |page|
    URI.join(url_str, page["image"])
  }
end

#def dynasty_scans_rip_series(url_str)
#  doc =

dynasty_oneshot_h = Handler.new 'https://dynasty-scans.com/chapters/'

def dynasty_oneshot_h.rip(url_str)
  return [dynasty_scans_rip_chapter(url_str)]
end

dynasty_series_h = Handler.new 'https://dynasty-scans.com/series/'

def dynasty_series_h.rip(url_str)
  doc = get_doc(url_str)
  doc.xpath('//dl[@class="chapter-list"]/dd/a[@class="name"]/@href').map { |elem|
    dynasty_scans_rip_chapter(URI.join url_str, elem.value)
  }
end
