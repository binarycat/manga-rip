require './common.rb'

# OLD METHOD, DO NOT USE
def manga4life_iterate(manga_name)
  ch_list = []
  done = false
  for ch_no in 1..9999 do
    page_list = []
    for page_no in 1..999 do
      ch_s = ch_no.to_s.rjust 4, '0'
      page_s = page_no.to_s.rjust 3, '0'
      # TODO: different manga uses different image hosts. figure out how to determine which to use. (the different sites seem to all run the same software though)
      url_str =
        "https://official.lowee.us/manga/#{manga_name}/#{ch_s}-#{page_s}.png"
      url = URI(url_str)
      p url_str
      resp = Net::HTTP.get_response(url)
      p resp
      if resp.code == "200" then
        #p 200
        page_list << url_str
      else
        if page_no == 0 then
          done = true
        end
        #p 404
        break
      end
    end
    ch_list << page_list
    if done then break end
  end
  return ch_list
end


# BUG: for certain manga (eg. 'How-Do-We-Relationship'), vm.CHAPTERS cuts off at a certain point.  a possible workarount is to instead iterate through the actual chapter list, or perhaps use the brute-force iteration method to find any chapters past that point (although the current brute force method misses half-chapters).
def manga4life_extract(manga_name)
  doc = get_doc("https://manga4life.com/read-online/#{manga_name}-chapter-1-page-1.html")
  doc.xpath('//script').each { |elem|
    domain = elem.inner_text.match /^\s*vm\.CurPathName = "(.*)";\r?$/
    if domain != nil then
      ch_json = elem.inner_text.match /^\s*vm.CHAPTERS = (.*);\r?$/
      return JSON::parse(ch_json[1]).each_with_index.map { |ch, ch_idx|
        (1..ch["Page"].to_i).map { |pg_no|
          # chapter numbers are stored in a very strange format
          ch_f = ch["Chapter"][1..-1].to_i / 10.0
          ch_i = ch_f.to_i
          # we pad the integer part with zeros, then append the decimal, if present.
          ch_s = ch_i.to_s.rjust(4, '0') 
          ch_d = ch_f-ch_i
          if ch_d != 0 then
            ch_s = ch_s + ch_d.to_s[1..-1]
          end
          # page numbers are thankfully normal
          pg_s = pg_no.to_s.rjust 3, '0'
          domain_s = domain[1]
          "https://#{domain_s}/manga/#{manga_name}/#{ch_s}-#{pg_s}.png"
        }
      }
    end
  }
  return nil
end

manga4life_h = Handler.new "https://manga4life.com/manga/"

def manga4life_h.rip(url_str)
  return manga4life_extract(url_str[29..-1])
end
