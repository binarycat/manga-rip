require './common.rb'

# webtoons is a fairly easy site to rip from, the only real annoyance is that
# when actually downloading the images, you must include the header of
# 'Referer: https://www.webtoons.com/'

# TODO: implement download method on handler which has the referer header

def webtoons_rip_episode(url_str)
  doc = get_doc(url_str)
  imgs = doc.xpath('//div[@id="_imageList"]/img/@data-url').map { |elem|
    elem.value
  }
  next_ep = doc.xpath('//a[@title="Next Episode"]/@href')
  if next_ep.length == 1 then
    next_ep = next_ep.first.value
  else
    next_ep = nil
  end
  return imgs, next_ep
end

def webtoons_rip_series(url_str)
  doc = get_doc(url_str)
  this_ep = doc.xpath('//a[@id="_btnEpisode"]/@href').inner_text
  ep_list = []
  while this_ep != nil do
    img_list, next_ep = webtoons_rip_episode(this_ep)
    ep_list << img_list
    this_ep = next_ep
  end
  return ep_list
end

webtoons_h = Handler.new "https://www.webtoons.com/"

def webtoons_h.rip(url_str)
  return webtoons_rip_series(url_str)
end

def webtoons_h.download_page(url)
  return spoof_referer(url, 'https://webtoons.com/')
end


