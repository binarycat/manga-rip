require './common.rb'

def mangabuddy_rip_chapter(url_str)
  doc = get_doc(url_str)
  doc.xpath("//script[not(@src)]").each { |elem|
    m = elem.inner_html.match /\s*var chapImages = '(.*)'/
    if m != nil then
      return m[1].split ","
    end
  }
end

mangabuddy_h = SpoofHandler.new "https://mangabuddy.com/"

def mangabuddy_h.rip(url_str) = rip_chapters(
  url_str, '//ul[@id="chapter-list"]/li/a/@href', :mangabuddy_rip_chapter)

