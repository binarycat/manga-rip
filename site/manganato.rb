require './common.rb'

def manganato_rip_chapter(url_str)
  doc = get_doc(url_str)
  return doc.xpath('//div[@class="container-chapter-reader"]/img/@src').values
end

manganato_h = SpoofHandler.new "https://manganato.com/"

def manganato_h.rip(url_str) = rip_chapters(
  url_str, '//a[@class="chapter-name text-nowrap"]/@href',
  :manganato_rip_chapter)
