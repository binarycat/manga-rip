require './common.rb'

def mangakakalot_rip_chapter(url_str)
  return get_doc(url_str).xpath('//div[@id="vungdoc"]/img/@data-src')
end

mangakakalot_h = SpoofHandler.new "https://ww6.mangakakalot.tv/manga/"

def mangakakalot_h.rip(url_str) = rip_chapters(
  url_str, '//div[@class="chapter-list"]/div/span/a/@href',
  :mangakakalot_rip_chapter)

