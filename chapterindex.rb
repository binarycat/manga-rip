# ChapterIndex is a lazy list.
# its function is to reduce the number of unneccary http calls
class ChapterIndex
  def initialize(list, &block)
    @src_list = list
    @dst_list = []
    @transform = block
  end
  def length = @src_list.length
  def [](idx)
    if @dst_list[idx] != nil then
      return @dst_list[idx]
    end
    return @dst_list[idx] = @transform.call(@src_list[idx])
  end
  # the new list will not have any of the cached data!
  def reverse
    return ChapterIndex.new(@src_list.reverse, &@transform)
  end
end
