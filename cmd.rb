#!/usr/bin/env ruby

require './main.rb'

subcmd = $*[0]

if subcmd in ["download", "dl"] then
  url_str = $*[1]
  outdir = $*[2]
  download_manga(url_str, outdir)
elsif subcmd == "sites" then
  get_supported_sites.each {|s|
    print s
    print "\n"
  }
else
  print "unsupported command!"
  exit 4
end
